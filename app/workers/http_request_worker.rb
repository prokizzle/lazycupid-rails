class HttpRequestWorker
  include Sidekiq::Worker

  def perform(link, user_id, request_id, callback)
    intended_handle = link.split('/').last
    p intended_handle
    begin
      user                  = User.find_by(id: user_id)
      cookies               = user.settings(:sessions).mechanize
      agent = Mechanize.new do |a|
        a.ssl_version = :TLSv1
      end
      agent.cookie_jar      = YAML::load(cookies)
      agent.user_agent      = user.settings(:personal).useragent
      url                   = URI.escape(link)
      agent.read_timeout    = 30
      page_object           = agent.get(link)
      page_object.encoding  = 'utf-8'
      # page_body             = page_object.parser.xpath("//body").to_html
      page_source           = page_object.parser.xpath("//html").to_s
      result_object        = {url: link.to_s, ready: true, username: intended_handle, source: page_source.to_s, inactive: false}
    rescue Exception => e
      result_object = {url: link, inactive: true, username: intended_handle, ready: true, error: e.message}
    end

    $redis.set(request_id, Marshal.dump(result_object))
    unless callback.nil?
      worker = Object.const_get(callback)
      worker.send(:perform_async, request_id, user_id)
    end

  end
end
