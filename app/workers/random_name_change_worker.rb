class RandomNameChangeWorker
  include Sidekiq::Worker

  def perform(id)
    offset = rand(UsernameChanges.count)
    @change = UsernameChanges.offset(offset).first
    RealtimeMessage.publish(id, 'name_change', {change: @change})
  end
end
