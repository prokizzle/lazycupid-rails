class RollWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  # sidekiq_options throttle: { threshold: 1, period: 30.seconds }
  sidekiq_options({retry: false})


  recurrence { minutely.second_of_minute(1, 20, 40) }

  def perform
    unless Roller.paused?
      User.find_each do |user|
        id = user.id
        account = Account.name(id)
        if Roller.enabled?(id)
          if Roller.should_roll_now?(user)
            if Roller.queue_size(id) == 0
              Roller.queue = {id: id, queue: Match.queue(id)}
            else
              _username = Roller.next(id)
              VisitorWorker.perform_async(_username, account, id) unless _username.nil?
            end
          end
        end
      end
    end
  end
end
