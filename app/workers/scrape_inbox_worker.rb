class ScrapeInboxWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: false do
    hourly
  end

  sidekiq_options({
                    retry: false,
                    queue: :default
  })

  def perform
    User.find_each do |user|
      if Roller.enabled?(user.id)
        request_id = UUIDTools::UUID.random_create
        Browser.request("http://www.okcupid.com/messages", user.id, request_id, ScrapeInboxHelperWorker)
      end
    end
  end
end
