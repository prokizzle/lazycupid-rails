class IgnoreMatchWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(account, name)
    Match.ignore(account, name)
  end
end
