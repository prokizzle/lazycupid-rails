class DuplicateDeletionWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :dupes

  def perform(name, account)
    Match.delete_duplicate(name, account)
  end
end

