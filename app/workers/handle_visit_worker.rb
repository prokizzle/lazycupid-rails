class HandleVisitWorker
  include Sidekiq::Worker

  sidekiq_options({
                    unique: true,
                    retry: false,
                    concurrency: 1,
                    expiration: 24 * 60 * 60
                    # throttle: { threshold: 1, key: ->(account){ account } }
  })

  def perform(result_id, id)
    result = Browser.response(result_id)
    account = Account.name(id)
    profile = Profile.parse(result)
    intended_name = result[:username]
    name    = profile[:name]

    # Check if user changed their name
    if profile[:handle] != profile[:intended_handle] && profile[:inactive] == false
      if profile[:a_list_name_change]
        Match.change_name(intended_name, profile[:new_handle])
        name = profile[:new_handle]
      end
    end

    # Set match as inactive if they closed their account
    if profile[:inactive]
      Match.where(name: intended_name).update_all(inactive: true)
    else
      HeadlessVisitWorker.perform_async(id, profile[:handle])
      match = Match.find_by(name: profile[:handle], account: account)
      unless match.nil?
        Match.ignore(account, name) if Match.should_ignore?(profile, id)
        update_match_details(match, profile)
        add_matches(match, account, profile, id)
        OutgoingVisit.create(name: match[:name], time: Time.now, date: Time.now, account: account, timestamp: Time.now.to_i)
      end
    end
  end

  def add_matches(match, account, profile, id)
    if Match.meets_preferences?(profile, id)
      profile[:similar_users].to_a.each do |user|
        AddMatchWorker.perform_async(account, user, profile[:gender], id, "#{account}_#{user.first}", {sexuality: profile[:sexuality], distance: profile[:distance], match_percent: 0})
      end
    end
  end

  def update_match_details(match, profile)
    # Increment the counter for number of times we've visited them
    match.increment!(:counts, by=1)

    # Update their record with information from the parsed profile
    match.update(
      # :name           => profile[:handle],
      :gender         => profile[:gender],
      :sexuality      => profile[:sexuality],
      :match_percent  => profile[:match_percent],
      :state          => profile[:state],
      :city           => profile[:city],
      # :height         => profile[:height],
      :last_online    => profile[:last_online],
      :last_visit     => Time.now.to_i,
      :time           => Time.now,
      :enemy_percent  => profile[:enemy_percent],
      :distance       => profile[:distance],
      :age            => profile[:age],
      :inactive       => profile[:inactive],
      :thumb          => profile[:thumb],
      :body_type      => Match.body_type_translator(profile[:bodytype])
    )

    match.save!
  end
end
