class VisitorWorker
  include Sidekiq::Worker
  sidekiq_options({
                    unique: true,
                    retry: false,
                    concurrency: 1,
                    expiration: 24 * 60 * 60
                    # throttle: { threshold: 1, key: ->(account){ account } }
  })

  def perform(name, account, id)
    url = "http://www.okcupid.com/profile/#{name}"
    request_id = UUIDTools::UUID.random_create
    Browser.request(url, id, request_id, HandleVisitWorker)
  end

end
