class HeadlessVisitWorker
  include Sidekiq::Worker

  def perform(id, username)
    HeadlessVisit.perform(id, username)
  end
end