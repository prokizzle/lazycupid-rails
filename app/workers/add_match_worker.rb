class AddMatchWorker
  include Sidekiq::Worker
  sidekiq_options({
                    :concurrency => 1,
                    :retry => false,
                    :unique => true
  })

  def perform(account, name, gender, id, okc_user_id, extra={})
    default = {account: account, name: name, unique_key: okc_user_id}
    Match.where(name: name).update_all(inactive: false) rescue nil
    if Match.add(default, gender, extra)
      RealtimeMessage.publish(id, 'new_match', {match: {name: name}})
    end
  end
end
