class VisitBackWorker
  include Sidekiq::Worker
  sidekiq_options({retry: false})


  def perform(request_id, user_id)
    ApiWatcherWorker.perform_async
    result = Browser.response(request_id)
    user = User.find_by(id: user_id)

    unless result[:source].nil?
      response = JSON.parse("{" + (result[:source].match(/\{(.+)\d+\}/)[1]) + "0}")
      p response
      events = response['events']
      events.each do |event|
        link = "http://okcupid.com/profile/#{event['from']}"
        request_id = UUIDTools::UUID.random_create
        Browser.request(link, user_id, request_id, DeleteRequest)
      end
    end
  end
end
