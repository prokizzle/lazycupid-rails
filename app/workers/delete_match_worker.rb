class DeleteMatchWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :deletions

  def perform(account)
    # Match.where(name: name, account: account).each{|m| m.delete}
    Match.delete_all(account: account)
  end
end

