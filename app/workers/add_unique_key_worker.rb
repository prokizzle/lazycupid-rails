class AddUniqueKeyWorker
  include Sidekiq::Worker

  def perform(name, account)
    match = Match.find_by(name: name, account: account)
    match.update(unique_key: "#{match.account}_#{match.name}")
    match.save
  end
end
