class StopAllJobsWorker
  include Sidekiq::Worker
  sidekiq_options({retry: true})

  def perform(id)
    user = User.find_by(id: id)
    account = Account.name(id)
    [
      Sidekiq::Queue.new(:default),
      Sidekiq::Queue.new(:rollers),
      Sidekiq::Queue.new(:dupes),
      Sidekiq::Queue.new(:deletions),
      Sidekiq::Queue.new(:add_users),
      Sidekiq::Queue.new(:master),
      Sidekiq::Queue.new(:inbox),
      Sidekiq::ScheduledSet.new
    ].each do |queue|
      queue.each{|job| job.delete if job.args.include?(account)}
    end
    RealtimeMessage.publish(id, 'roll_status', {status: false})
  end
end
