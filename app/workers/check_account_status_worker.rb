class CheckAccountStatusWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options({retry: false})

  recurrence { hourly }

  def perform
    User.find_each do |user|
      if Roller.enabled?(user.id)
        browser   = Browser.new(session: OkcupidSessions.mechanize(user.id))
        headless  = HeadlessBrowser.new(session: OkcupidSessions.headless(user.id))
        unless browser.is_logged_in? && headless.is_logged_in?
          Roller.disable!(user.id)
          Roller.clear_session(user.id)
        end
      end
    end
  end
end
