class BrowserWorker
      include Sidekiq::Worker

      def perform(url, id)
            user = User.find_by(id: id)
            browser = Browser.new(session: user.settings(:roller).session)
            result = Hash.new
            request_id = Time.now.to_i
            browser.send_request(url, request_id)
            until result[:ready] == true
                  result = browser.get_request(request_id)
            end
            browser.delete_response(request_id)
            user.settings(:roller).session = browser.save_session
            user.save
            result
      end
end
