class ScrapeInboxHelperWorker
  include Sidekiq::Worker

  sidekiq_options({
                    retry: false,
                    queue: :default
  })

  def perform(request_id, id)
    InboxParser.perform_async(request_id, id)
  end
end
