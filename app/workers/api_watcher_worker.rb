class ApiWatcherWorker
  include Sidekiq::Worker

  sidekiq_options({retry: true})

  def perform
    User.find_each do |user|
      if Roller.enabled?(user.id)
        if $rollout.active?(:visit_back, user)
          i = (1..4).to_a.shuffle.sample
          link = "http://#{i}-instant.okcupid.com/instantevents?random=#{rand}&server_gmt=#{Time.now.to_i}"
          request_id = UUIDTools::UUID.random_create
          Browser.request(link, user.id, request_id, VisitBackWorker)
        end
      end
    end
  end
end
