class MatchPageScraperHelperWorker
  include Sidekiq::Worker

  sidekiq_options({retry: false})

  def perform(request_id, id)
    MatchPageScraper.perform_async(id, request_id)
  end
end
