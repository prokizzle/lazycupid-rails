class JsonRequestWorker
  include Sidekiq::Worker

  def perform(link, user_id, request_id, callback)
    intended_handle = link.split('/').last
    p intended_handle
    begin
      user                  = User.find_by(id: user_id)
      cookies               = user.settings(:sessions).mechanize
      agent = Mechanize.new do |a|
        a.ssl_version = :TLSv1
      end
      agent.cookie_jar      = YAML::load(cookies)
      agent.user_agent      = user.settings(:personal).useragent
      url                   = URI.escape(link)
      agent.read_timeout    = 30
      page_object           = agent.get(link)
      result_object         = JSON.parse(page_object.body)
    rescue Exception => e
      result_object = {error: true, message: e.message}
    end

    $redis.set(request_id, Marshal.dump(result_object))
    unless callback.nil?
      worker = Object.const_get(callback)
      worker.send(:perform_async, request_id, user_id)
    end

  end
end
