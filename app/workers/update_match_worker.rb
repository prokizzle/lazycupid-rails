class UpdateMatchWorker
  include Sidekiq::Worker
  sidekiq_options({retry: false})

  def perform(account, profile, id)
    match = Match.find_by(name: profile[:handle], account: account)
    if profile[:inactive]
      Match.where(name: name).update_all(inactive: true)
    else
      Match.ignore(account, name) if Match.should_ignore?(profile, id)

      # Increment the counter for number of times we've visited them
      match.increment!(:counts, by=1)

      # Update their record with information from the parsed profile
      match.update(
        # :name           => profile[:handle],
        :gender         => profile[:gender],
        :sexuality      => profile[:sexuality],
        :match_percent  => profile[:match_percent],
        :state          => profile[:state],
        :city           => profile[:city],
        :height         => profile[:height],
        :last_online    => profile[:last_online],
        :last_visit     => Time.now.to_i,
        :time           => Time.now,
        :enemy_percent  => profile[:enemy_percent],
        :distance       => profile[:distance],
        :age            => profile[:age],
        :inactive       => profile[:inactive],
        :thumb          => profile[:thumb],
        :body_type      => Match.body_type_translator(profile[:bodytype])
      )

      match.save!

    end
    OutgoingVisit.create(name: match[:name], time: Time.now, date: Time.now, account: account, timestamp: Time.now.to_i)

  end
end
