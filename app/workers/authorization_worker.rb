class AuthorizationWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  sidekiq_options({
                    retry: false
  })


  recurrence { hourly.minute_of_hour(0, 30) }

  def perform

    User.find_each do |user|
      if Roller.disabled?(user.id)
        user.settings(:roller).status = false
      else
        user.settings(:roller).status = true
      end
      user.save
      $redis.hset("roll_enabled", user.id, user.settings(:roller).status)
    end
  end
end
