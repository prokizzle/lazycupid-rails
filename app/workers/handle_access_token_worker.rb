class HandleAccessTokenWorker
  include Sidekiq::Worker

  def perform(request_id, id)
    prefs = Match.preferences(id)
    if prefs[:sexuality] == 'straight'
      if prefs[:gender] == "M"
        they_want = 'men'
        i_want = 'women'
      else
        they_want = 'women'
        i_want = 'men'
      end
    else
      if prefs[:gender] == "M"
        they_want = 'men'
        i_want = 'men'
      else
        they_want = 'women'
        i_want = 'women'
      end
    end
    result = Browser.response(request_id)
    @html = Mechanize::Page.new(nil,{'content-type'=>'text/html'},result[:source],nil,Mechanize.new)
    access_token = @html.parser.search('script')[1].text.match(/ACCESS_TOKEN\s*=\s*"(.+)"/)[1]

    match_options = URI.encode({order_by: "LOGIN", last_login:3600, limit: 1000}.to_json)

    match_url = "https://www.okcupid.com/apitun/match/search?&access_token=#{access_token}&_json=#{match_options}"

    request_id = UUIDTools::UUID.random_create
    Browser.request_json(match_url, id, request_id, MatchPageScraper)
  end
end
