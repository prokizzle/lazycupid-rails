class MatchPageScraperWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  recurrence backfill: false do
    hourly
  end
  sidekiq_options({
                    :retry => false
  })

  def perform
    User.find_each do |user|
      if Roller.enabled?(user.id)
        MatchPageScraper.init(user.id)
      end
    end
  end
end
