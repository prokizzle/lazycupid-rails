class DeleteRequest

  def self.perform_async(request_id, user_id)
    $redis.del(request_id)
  end
end