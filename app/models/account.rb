class Account
  def self.name(id)
    account = $redis.hget("accounts", id)
    if account.nil? || account.empty?
      account = User.find_by(id: id).settings(:personal).account
      $redis.hset("accounts", id, account)
    end
    return account
  end
end
