class OkcupidSessions

  def self.headless(id)
    response = $redis.hget('headless_session', id)
    if response.nil? || response.empty?
      user = User.find_by(id: id)
      response = user.settings(:sessions).headless
      $redis.hset('headless_session', id, response)
    end
    return response
  end

  def self.mechanize(id)
    response = $redis.hget('mechanize_session', id)
    if response.nil? || response.empty?
      response = User.find_by(id: id).settings(:sessions).mechanize
      $redis.hset('mechanize_session', id, response)
    end
    return response
  end

  def self.redis_cache(key, id)
    response = $redis.hget(key, id)
    if response.nil? || response.empty?
      response = User.find_by(id: id).settings(:sessions).send(key.split('_').first.to_sym)
      $redis.hset(key, id, response)
    end
    return response
  end
end
