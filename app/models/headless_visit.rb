class HeadlessVisit
  def self.perform(id, username)
    headless = HeadlessBrowser.new(session: OkcupidSessions.headless(id))
    sleep 2
    headless.visit("https://www.okcupid.com/profile/#{username}")
    sleep 3
    headless.browser.close
  end
end