# encoding: utf-8

# OKCupid profile page parser
# Class methods for turning a scraped Mechanize page for an OKCupid user profile
# into a hash of attributes for easy data manipulation and storage.
#
class Profile
  require 'lingua'
  require 'nikkou'

  attr_reader :url, :html, :intended_handle, :result

  public

  def self.test(id)
    request_id = UUIDTools::UUID.random_create
    Browser.request('http://okcupid.com/profile/you_bet_girraffe', id, request_id, nil)
    sleep 10
    @result = Browser.response(request_id)
    p parse(@result)
  end

  def self.save_session(id, session)
    user    = User.find_by(id: id)
    user.settings(:roller).session = session
    user.save
  end

  # Parses a profile page for user attributes
  #
  # @param user_page [Hash] [A browser request hash containing body, url, and Mechanize page object]
  # @return [Hash] a hash of attributes scraped from the profile page
  #
  def self.parse(result)
    @html             = Mechanize::Page.new(nil,{'content-type'=>'text/html'},result[:source],nil,Mechanize.new)

    inactive = result[:inactive]
    enhanced_privacy = !(result[:source] =~ $enhanced_privacy).nil?
    @intended_handle = result[:username]

    # @analyze = TextClassification.new(read_key: $uclassify_read_key, text: essays)

    if inactive
      {inactive: true, message: result[:error]}
    elsif enhanced_privacy
      return {handle: @intended_handle,
              sexuality: "Straight"}
    else
      {
        # essays: essays,
        a_list_name_change: @intended_handle.downcase != ajax_field('basic_info_sn').downcase,
        age: ajax_field('ajax_age').to_i,
        age_range: user_age_range,
        bodytype: ajax_field('ajax_bodytype'),
        city: city,
        distance: relative_distance,
        drinking: drinking,
        drugs: attr_contains?('ajax_drugs', ['Often', 'Sometimes']),
        smoking: smoking,
        enemy_percent: enemy_percentage,
        ethnicity: ajax_field('ajax_ethnicities'),
        flesch: readability[:flesch],
        fog: readability[:fog],
        gender: gender,
        handle: ajax_field('basic_info_sn'),
        # height: height,
        inactive: false,
        intended_handle: @intended_handle,
        kids: kids?,
        asian: attr_contains?('ajax_ethnicities', "Asian"),
        kincaid: readability[:kincaid],
        last_online: last_online,
        location: ajax_field('ajax_location'),
        match_percent: match_percentage,
        new_handle: ajax_field('basic_info_sn'),
        relationship_status: relationship_status,
        sexuality: sexuality,
        similar_users: similar_users,
        state: state,
        thumb: thumb,
        single: single?
      }
      # sentiment: @analyze.sentiment,
      # mood: @analyze.mood(essays),
      # perceived_gender: @analyze.gender(essays),
      # perceived_age: @analyze.age(essays),
      # body: @body,
      # html: @html }
    end

  end

  private

  def self.readability
    @object   ||= Lingua::EN::Readability.new(essays)
    @fog      ||= @object.fog.ceil rescue nil
    @flesch   ||= @object.flesch.ceil rescue nil
    @kincaid  ||= @object.kincaid.ceil rescue nil
    return {
      kincaid:  @kincaid,
      fog:      @fog == 'NaN' ? nil : @fog,
      flesch:   @flesch == 'NaN' ? nil : @flesch
    }
  end

  def self.thumb
    @html.parser.xpath('//*[@id="thumb0_a"]/img').first.first[1] rescue nil
  end

  def self.similar_users
    @html.parser.attr_equals('class', 'user_image').map do |u|
      u[:href].split('/')[2].split('?')[0]
    end
  end

  def self.user_age_range
    target = ajax_field('ajax_ages').split(/\D/).select{|a| !a.empty?}
    return {min_age: target[0].to_i, max_age: target[1].to_i}

  end

  # match percentages

  def self.match_percentage(try=0)
    ajax_field('match .percent').split(/\D/)[0].to_i
  end

  def self.enemy_percentage
    ajax_field('enemy .percent').split(/\D/)[0].to_i
  end

  # user details

  def self.ajax_field(id)
    target, tag_types = nil, ['#', '.']
    until target || tag_types.empty?
      target = @html.parser.find("#{tag_types.shift}#{id}")
    end
    target ? target.text.strip : ''
  end

  def self.attr_contains?(id, options=[], expected=true)
    options.include?(ajax_field(id)) == expected
  end

  def self.height
    target = ajax_field('ajax_height')
    target ? /(\d+.\d+)../.match(target)[1].to_f : 0.0
  end

  def self.smoking
    attr_contains?('ajax_smoking', ['Often', 'Sometimes', 'When drinking'])
  end

  def self.drinking
    attr_contains?('ajax_drinking', 'Never', false)
  end

  def self.kids?
    attr_contains?('ajax_children', "Doesn’t have kids", false)
  end

  def self.last_online
    target = @html.parser.find('.fancydate')
    target.nil? ? 0 : target.time.to_i
  end

  def self.city
    target = ajax_field('ajax_location')
    target ? target.split(',')[0].strip : ''
  end

  def self.state
    target = ajax_field('ajax_location')
    target ? target.split(',')[1].strip : ''
  end

  def self.sexuality
    target = ajax_field('ajax_orientation')
    target ? target.split.first.split(',').first : 'Straight'
  end

  def self.gender
    target = ajax_field('ajax_gender').strip.split(',').first
    target == "Woman" ? "F" : "M"
  end

  def self.relative_distance
    ajax_field('dist').split(/\D/)[1].to_i
  end

  def self.relationship_status
    ajax_field('ajax_status')
  end

  def self.single?
    attr_contains?('ajax_status', 'Single')
  end

  def self.essays
    [ajax_field('essay_text_0'),
     ajax_field('essay_text_1'),
     ajax_field('essay_text_2'),
     ajax_field('essay_text_3'),
     ajax_field('essay_text_6'),
     ajax_field('essay_text_7'),
     ajax_field('essay_text_8'),
     ajax_field('essay_text_9')].join("\n")
  end
end
