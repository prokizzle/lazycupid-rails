class MatchPageScraper
  def self.run(result, id)
    account = Account.name(id)
    result["data"].each do |m|
      gender = m["gender"] == 1 ? "M" : "F"
      if prefs[:sexuality] == "gay"
        if prefs[:gender] == 'M'
          acceptable_gender = "M"
        else
          acceptable_gender = "F"
        end
      elsif prefs[:sexuality] == 'straight'
        if prefs[:gender] == 'M'
          acceptable_gender = 'F'
        else
          acceptable_gender = 'M'
        end
      end

      case m["orientation"].to_i
      when 1
        sexuality = "Straight"
      when 2
        sexuality = "Gay"
      else
        sexuality = "Bisexual"
      end
      if gender == acceptable_gender
        match = m['match'].to_i/100
        AddMatchWorker.perform_async(account, m['username'], gender, id, "#{account}_#{m['username']}", {age: m['age'], city: m['city_name'], state: m['state_code'], match_percent: match.to_i/100, sexuality: sexuality})
      end
    end
  end

  def self.init(id)
    request_id = UUIDTools::UUID.random_create
    Browser.request(MatchQueries.default_query, id, request_id, HandleAccessTokenWorker)
  end

  def self.headless_scrape(id)
    headless = HeadlessBrowser.new(session: OkcupidSessions.headless(id))
    headless.browser.divs(:class => 'user-not-hidden').each do |card|
    end
  end

  def self.test(id)
    request_id = UUIDTools::UUID.random_create
    Browser.request(MatchQueries.default_query, id, request_id, nil)
    sleep 5
    result = Browser.response(request_id)
    run(result, id)
  end

  def self.perform_async(request_id, id)
    result = Browser.response(request_id, true)
    run(result, id)
  end
end
