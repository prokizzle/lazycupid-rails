class Subscription
  def self.create(user)
    begin
      customer = Billing.customer(user)
      customer.subscriptions.create(plan: "basic")
      error, message = false, ""
      user.subscribed = true
      user.save
    rescue Exception => e
      error = true
      message = e.message
    end
    return {user: Billing.customer(user), error: error, message: message}
  end
end
