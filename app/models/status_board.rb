class StatusBoard
  def self.user_statuses
    data = []
    User.find_each do |user|
      data << [Account.name(user.id), status_message(Roller.enabled?(user.id))]
    end
    table = PanicBoardData::Table.new data
    table.widths = [nil, 150]
    table.to_html
  end

  def self.outgoing_visits
    heading = "Visits This Week"
    value = PanicBoardData::SingleValue.new heading, OutgoingVisit.where("created_at > ?", Time.now-7.days).size
    return value.to_html
  end

  def self.statuses
    messages = PanicBoardData::DataSequence.new('Messages')
    visits = PanicBoardData::DataSequence.new('Visits')

    User.find_each do |user|
      if Roller.enabled?(user.id)
        account = Account.name(user.id)
        messages.data[account] = IncomingMessage.where(account: account).where("created_at > ?", Time.now-7.days).size
        visits.data[account] = OutgoingVisit.where(account: account).where("created_at > ?", Time.now-7.days).size
      end
    end
    graph = PanicBoardData::Graph.new
    graph.title = "Success Rates"

    graph.type = :bar

    graph.data_sequences << visits
    graph.data_sequences << messages

    graph.to_json
  end

  def self.incoming_messages
    heading = "Messages This Week"
    value = PanicBoardData::SingleValue.new heading, IncomingMessage.where("created_at > ?", Time.now-7.days).size
    return value.to_html
  end

  def self.processes
    data = []
    ps = Sidekiq::ProcessSet.new
    ps.each do |process|
      data << [process['queues'].first, process['busy']]
    end
    table = PanicBoardData::Table.new data
    table.widths = [nil, 80]
    table.to_html
  end

  def self.distances
    data = []
    User.find_each do |user|
      data << [Account.name(user.id), user.settings(:match_settings).max_distance]
    end
    table = PanicBoardData::Table.new data
    table.widths = [nil, 80]
    table.to_html
  end

  def self.status_message(msg)
    msg == true ? "Active" : "Inactive"
  end

  def self.sidekiq
    data = []
    data << ['Default' ,Sidekiq::Queue.new(:defualt).size]
    data << ['High' ,Sidekiq::Queue.new(:high).size]
    data << ['Low' ,Sidekiq::Queue.new(:low).size]
    table = PanicBoardData::Table.new data
    table.widths = [nil, 80]
    table.to_html
  end

  def self.roller_queues
    data = []
    queue = PanicBoardData::DataSequence.new('Queue')

    User.find_each do |user|
      if Roller.enabled?(user.id)
        queue.data[Account.name(user.id)] = Roller.queue_size(user.id)
      end
    end
    graph = PanicBoardData::Graph.new
    graph.title = "Queue Size"

    graph.type = :bar

    graph.data_sequences << queue

    graph.to_json
  end
end
