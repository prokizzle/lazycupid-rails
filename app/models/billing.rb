class Billing

  def self.customer(customer)
    stripe_token = nil
    unless customer.nil? || customer.stripe_id.nil? || customer.stripe_id.empty?
      begin
        stripe_token = Stripe::Customer.retrieve(customer.stripe_id)
      rescue Exception => e
        if e.message.has_text?("object exists in test mode")
          customer.stripe_id = nil
          customer.save
        end
      end
    end

    if stripe_token.nil?
      stripe_token = create(customer)
    end

    return stripe_token
  end

  def self.create(user)
    stripe_customer_object  = Stripe::Customer.create(email: user.email, description: "Subscription for #{user.email}")
    user.stripe_id = stripe_customer_object.id
    user.save
    return stripe_customer_object
  end
end
