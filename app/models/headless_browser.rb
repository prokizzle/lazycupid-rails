class HeadlessBrowser
  attr_accessor :browser

  def initialize(params)
    @username = params[:username]
    @password = params[:password]
    # driver = Webdriver::UserAgent.driver(:browser => :chrome, :agent => :iphone, :orientation => :landscape)
    @browser = Watir::Browser.new(:phantomjs)
    @base_url = "http://www.okcupid.com"
    if params.has_key?(:session)
      load_session(params[:session])
    else
      login
    end

  end

  def is_logged_in?
    visit('https://okcupid.com')
    @browser.html.include?('logged_in')
  end

  def visit(url)
    begin
      @browser.goto(url)
    rescue EOFError
      @browser.goto(url)
    end
  end

  def login
    @browser.goto("http://www.okcupid.com")
    # @browser.screenshot.save 'pre-login.png' rescue nil
    @browser.link(:text => "Sign in").click
    sleep (1..3).to_a.sample.to_i
    @browser.text_field(:id => 'login_username').set(@username)
    sleep (1..3).to_a.sample.to_i
    @browser.text_field(:id => 'login_password').set(@password)
    sleep (1..3).to_a.sample.to_i
    @browser.button(:id => 'sign_in_button').click
    sleep (1..3).to_a.sample.to_i
    return is_logged_in?
  end

  def save_session
    YAML::dump(@browser.cookies.to_a)
  end

  def load_session(session)
    saved_cookies = YAML::load(session)
    visit("https://okcupid.com")
    @browser.cookies.clear
    saved_cookies.each do |saved_cookie|
      @browser.cookies.add(saved_cookie[:name], saved_cookie[:value])
    end
    # visit("https://okcupid.com")
  end
end
