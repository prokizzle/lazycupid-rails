class IncomingMessage < ActiveRecord::Base
  # has_one :match

  def self.delete_duplicate(message_id, account)
    message = where(message_id: message_id, account: account)
    size = message.size
    count = 1
    message.reverse_each do |m|
      unless count == size
        m.delete
        count += 1
      end
    end
  end
end
