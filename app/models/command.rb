class Command
  def self.parse(params)
    com = params["Body"].split(" ")
    id = com[0]
    command = com[1] || nil
    arg = com[2] || nil
    user = User.find_by(id: id)
    account =  user.settings(:personal).account
    if command == "status"
      status = Roller.enabled?(com[0]) ? "Active" : "Inactive"
    elsif command == "distance"
      unless arg.nil?
        user.settings(:match_settings).max_distance = arg.to_i
        user.save
        status = "Max distance - #{arg}"
      else
        status = user.settings(:match_settings).max_distance
      end
    elsif command == "scrape"
      status = "Added: #{Match.scrape(id).size} users"
    elsif command == "creep"
      Match.creep_factor(id, arg.to_i)
      status = "Creep factor updated"
    elsif command == "enable"
      Roller.enable!(id)
      status = "enabled"
    elsif command == "disable"
      Roller.disable!(id)
      status = "disabled"
    elsif command == "size"
      stats = Sidekiq::Queue.new(:default)
      status = stats.size
    elsif command == "clear"
      stats = Sidekiq::Queue.new(:default)
      until stats.size == 0
        stats.clear
      end
    else
      status = "#{Roller.queue_size(com[0])} queued"
    end
    return {status: status, account: account}
  end
end
