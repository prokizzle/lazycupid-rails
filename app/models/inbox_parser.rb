require 'highline/import'
class InboxParser

  def self.browser(id)
    user = User.find_by(id: id)
    browser = Browser.new(session: user.settings(:roller).session)
  end

  def self.test_request(url)
    user = User.find_by(id: 1)
    browser = Browser.new(session: user.settings(:roller).session)
    @result = Hash.new
    request_id = Time.now.to_i
    browser.send_request(url, request_id)
    until @result[:ready] == true
      @result = browser.get_request(request_id)
    end
    browser.delete_response(request_id)
    return @result
  end

  def self.async_response(url, id)
    result = Hash.new
    request_id = Time.now.to_i
    cached_browser = browser(id)
    cached_browser.send_request(url, request_id)
    until result[:ready] == true
      result = cached_browser.get_request(request_id)
    end
    cached_browser.delete_response(request_id)
    result
  end

  def self.perform_async(request_id, id)
    scrape_inbox(request_id, id)
  end

  def self.scrape_inbox(request_id, id)
    user = User.find_by(id: id)
    account = Account.name(id)
    @most_recent_message_id = IncomingMessage.where(account: account).order(timestamp: :desc).first.message_id rescue 0
    @inbox_up_to_date = false
    result = Browser.response(request_id)

    unless result[:source].empty? || @inbox_up_to_date
      low = 1
      extract_messages_from_page(id, low, @most_recent_message_id, account)
      until @inbox_up_to_date
        low += 30
        @inbox_up_to_date = extract_messages_from_page(id, low, @most_recent_message_id, account)
        sleep (1..6).to_a.sample.to_i
      end
    end
    return "done"
  end

  def self.cache_read_messages(id)
    msgs_to_queue(id, 'li.readMessage', 'msg_queue')
  end

  def self.read_messages_size(id)
    $redis.llen("#{id}_msg_queue")
  end

  def self.unread_messages_size(id)
    $redis.llen("#{id}_new_msg_queue")
  end

  def self.cache_unread_messages(id)
    msgs_to_queue(id, 'li.unreadMessage', 'new_msg_queue')
  end

  def self.cache_mutual_match_messages(id)
    mutual_match_msgs(id)
  end

  def self.cache_all_messages(id)
    msgs_to_queue(id, 'li.thread', 'all_msg_queue')
  end

  def self.msgs_to_queue(id, selector, queue_name)
    low, msgs_on_page = 1, 1
    until msgs_on_page == 0
      result = async_response("http://www.okcupid.com/messages?low=#{low}&infiniscroll=1&folder=1", id)
      messages = result[:html].search(selector)
      messages.each{|m| $redis.rpush("#{id}_#{queue_name}", m.search('.open')[0].attributes["href"].value)}
      msgs_on_page = result[:body].scan(/"message_(\d+)"/).size
      low += 30
    end
  end

  def self.mutual_match_msgs(id)
    low, msgs_on_page = 1, 1
    until msgs_on_page == 0
      result = async_response("http://www.okcupid.com/messages?low=#{low}&infiniscroll=1&folder=1", id)
      messages = result[:html].search('.previewline').text_equals("It's a match!")
      messages.each{|m| $redis.rpush("#{id}_mutual_matches", m.parent.attributes["href"].value)}
      msgs_on_page = result[:body].scan(/"message_(\d+)"/).size
      low += 30
    end
  end

  def self.ttest
    result = async_response("http://www.okcupid.com/messages?low=1&infiniscroll=1&folder=1", 7)
    return result[:html].search('.previewline').text_equals("It's a match!").first.parent.attributes["href"].value
  end

  def self.next_message(id, queue, cache_method)
    nextMsg = $redis.rpop("#{id}_#{queue}")
    if nextMsg.nil?
      send(cache_method, id)
      nextMsg = $redis.rpop("#{id}_#{queue}")
    end
    return {nextMsg: nextMsg, remaining: $redis.llen("#{id}_#{queue}").to_i}
  end

  def self.robot(id)
    _next = next_message(id, 'msg_queue', "cache_read_messages")
    Launchy.open "https://www.okcupid.com#{_next[:nextMsg]}"
  end

  def self.extract_messages_from_page(id, low, most_recent_message_id, account)
    @inbox_up_to_date = false
    # delete_mutual_matches(msg_page)

    result = async_response("http://www.okcupid.com/messages?low=#{low}&infiniscroll=1&folder=1", id)
    unless result[:body].empty?
      message_list = result[:body].scan(/"message_(\d+)"/)
      @total_msg_on_page = message_list.size
      unless @total_msg_on_page == 0
        unless message_list.include?(most_recent_message_id)
          message_list.each do |message_id|
            message_id      = message_id.first
            if message_id == @most_recent_message_id
              @inbox_up_to_date = true
              break
            end
            msg_block       = result[:html].parser.xpath("//li[@id='message_#{message_id}']").to_html
            sender          = msg_block.match($inbox_handle)[1].to_s
            timestamp       = msg_block.match(/(\d{10}), 'BRIEF/)[1].to_i
            register_message(account, sender, timestamp, message_id)
          end
        else
          @inbox_up_to_date = true
        end
      else
        @inbox_up_to_date = true
      end
      return @inbox_up_to_date
    else
      return true
    end
  end

  def self.register_message(account, sender, timestamp, message_id)

    # [todo] - add messages table to db
    # [todo] - register message with timestamp, account, sender, and message id
    # [todo] - check to see if this message is unique/new

    begin
      IncomingMessage.create(account: account, username: sender, timestamp: timestamp, message_id: message_id)
    rescue => e
    end
    Match.ignore(account, sender)
  end
end
