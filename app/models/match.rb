#encoding: utf-8

class Match < ActiveRecord::Base
  has_attached_file :photo

  validates :name, uniqueness: {scope: [:account, :unique_key]}
  # after_create do |m|
  #   delete_duplicate(m.name, m.account)
  # end

  def photo_from_url(url)
    self.photo = open(url)
  end

  def self.queue(id)
    return query(id).reject{|m| m.match_percent == 0 && m.match_percent < m.enemy_percent}.take(100).map{|m| m.name}.shuffle
  end

  def self.offset_minimum_time(min_time)
    offset = [
      7.hours.ago, 4.hours.ago, 2.hours.ago, 35.minutes.ago,
      12.minutes, 36.minutes, 90.minutes, 124.minutes,
    225.minutes, 6.hours, 10.hours].map{|m| m.to_i}
    return 0..(min_time + offset.shuffle.sample)
  end

  def self.query(id)
    prefs = preferences(id)
    match_percent_range = [0]
    (prefs[:min_match_percent]..100).to_a.each{|n| match_percent_range.push(n)}


    conditions = {:account => prefs[:account],
                  :ignored => false,
                  :inactive => false,
                  :distance => 0..prefs[:max_distance],
                  :age => prefs[:min_age]..prefs[:max_age],
                  :last_visit => offset_minimum_time(prefs[:min_time]),
                  :counts => 0..prefs[:max_visits],
                  :body_type => prefs[:body_types],
                  :match_percent => match_percent_range}
    return query_by_gender(genders_to_visit(id), conditions)
  end

  def self.update(profile)
    match = where(name: profile[:name], account: session[:account]).first_or_create
    details = {
      age: profile[:age],
      city: profile[:city],
      state: profile[:state],
      gender: profile[:gender],
      match_percent: profile[:match_percent],
      enemy_percent: profile[:enemy_percent],
      sexuality: profile[:sexuality],
      height: profile[:height],
      last_visit: Time.now.to_i,
      thumb: profile[:profile_picture],
      distance: profile[:distance],
      inactive: profile[:inactive]

    }
    match.update(details)
    match.counts += 1
    match.save
  end

  def self.body_type_translator(body_type)
    types = {
      "A little extra" => "large",
      "Average" => "medium",
      "Curvy" => "medium",
      "Full figured" => "large",
      "Voluptuous" => "large",
      "Overweight" => "large",
      "Thin" => "small",
      "Fit" => "small",
      "Athletic" => "small",
      "Skinny" => "small",
      "Rather not say" => "large",
      "Jacked" => "medium",
      "Used up" => "small",
      "-" => "small",
      "—" => "small"
    }
    return types[body_type]
  end


  def self.preferences(id)
    user = User.find_by(id: id)
    body_types = [nil]
    body_types.push "large" if user.settings(:match_settings).body_large
    body_types.push "medium" if user.settings(:match_settings).body_medium
    body_types.push "small" if user.settings(:match_settings).body_small
    {account: Account.name(id),
     sexuality: user.settings(:personal).sexuality,
     gender: user.settings(:personal).gender,
     my_age: user.settings(:personal).age,
     age: user.settings(:personal).age.to_i,
     min_age: user.settings(:match_settings).min_age.to_i,
     max_visits: user.settings(:match_settings).max_visits.to_i,
     max_age: user.settings(:match_settings).max_age.to_i,
     max_distance: user.settings(:match_settings).max_distance.to_i,
     min_match_percent: user.settings(:match_settings).min_match_percent.to_i,
     body_types: body_types,
     min_time: Chronic.parse("#{user.settings(:match_settings).min_days_ago.to_i} days ago").to_i}
  end

  def self.creep_factor(id, value)
    opts = {}
    case value.to_i
    when 1
      opts = {max_visits: 20, min_days_ago: 9, min_match_percent: 80}
    when 2
      opts = {max_visits: 30, min_days_ago: 7, min_match_percent: 70}
    when 3
      opts = {max_visits: 40, min_days_ago: 5, min_match_percent: 60}
    when 4
      opts = {max_visits: 50, min_days_ago: 3, min_match_percent: 50}
    when 5
      opts = {max_visits: 60, min_days_ago: 2, min_match_percent: 1}
    end
    user = User.find_by(id: id)
    user.settings(:match_settings).max_visits = opts[:max_visits]
    user.settings(:match_settings).min_days_ago = opts[:min_days_ago]
    user.settings(:match_settings).min_match_percent = opts[:min_match_percent]
    user.settings(:match_settings).creep_factor = value
    user.save
  end




  def self.delete_duplicate(match, account)
    match = where(name: match, account: account)
    size = match.size
    count = 1
    match.reverse_each do |m|
      unless count == size
        m.delete
        count += 1
      end
    end
  end

  def self.genders_to_visit(id)
    user = User.find_by(id: id)
    gender = user.settings(:personal).gender
    sexuality = user.settings(:personal).sexuality
    if sexuality == "straight"
      if gender == "M"
        genders = ["F"]
        sexualities = {"F" => ["Straight", "Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", nil]}
      else
        genders = ["M"]
        sexualities = {"M" => ["Straight", "Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", nil]}
      end

    elsif sexuality == "bisexual"
      if gender == "M"
        sexualities = {"M" => ["Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", "Gay", nil], "F" => ["Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", "Straight", nil]}
      else
        sexualities = {"F" => ["Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", "Gay", "Lesbian"], "M" => ["Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", "Straight", nil] }
      end
      genders = ["F", "M"]

    elsif sexuality == "gay"
      if gender == "M"
        genders = ["M"]
        sexualities = {"M" => ["Gay", "Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", nil]}
      else
        genders = ["F"]
        sexualities = {"F" => ["Gay", "Bisexual", "Sapiosexual", "Pansexual", "Homoflexible", "Straight", "Heteroflexible", "Asexual", "Questioning", "Demisexual", "Lesbian", nil]}
      end
    end
    # genders.push(nil)
    return {genders: genders, sexualities: sexualities}
  end

  def self.query_by_gender(genders_obj, conditions)
    queries = []
    final_query = {}
    genders_obj[:genders].each do |gender|
      unless gender.nil?
        gender_conditions = {gender: [gender, nil], sexuality: genders_obj[:sexualities][gender]}
        queries.push(where(conditions.merge(gender_conditions)).order(match_percent: :desc))
      end
    end
    first_query = queries.shift
    queries.each {|q| first_query.merge!(q)}
    return first_query
  end

  def self.meets_preferences?(profile, id)
    prefs = preferences(id)
    # unless height_criteria_met?
    #   Match.ignore(@user[:handle], my_prefs.account)
    # end
    # age_range_criteria_met?(profile, prefs) &&
    # TODO: Fix age range

    match_percent_criteria_met?(profile, prefs) &&
      distance_criteria_met?(profile, prefs) &&
      age_criteria_met?(profile, prefs) &&
      sexuality_criteria_met?(profile, genders_to_visit(id)) &&
      body_type_criteria_met?(profile, prefs)
  end

  def self.age_range_criteria_met?(profile, prefs)
    in_range = (profile[:age_range][:min_age]..profile[:age_range][:max_age]).to_a.include?(prefs[:age])
    is_cougar = (profile[:age] - profile[:age_range][:min_age]) > 9
    return (in_range || is_cougar)
  end

  def self.match_percent_criteria_met?(profile, prefs)
    (profile[:match_percent] >= prefs[:min_match_percent] || (profile[:match_percent] == 0 && profile[:enemy_percentage] == 0))
  end

  def self.distance_criteria_met?(profile, prefs)
    profile[:distance] <= prefs[:max_distance]
  end

  def self.age_criteria_met?(profile, prefs)
    profile[:age].between?(prefs[:min_age], prefs[:max_age])
  end

  def self.height_criteria_met?(profile, prefs)
    (profile[:height].to_f >= prefs[:min_height] && profile[:height].to_f <= prefs[:max_height]) || profile[:height] == 0
  end

  def self.body_type_criteria_met?(profile, prefs)
    prefs[:body_types].include? body_type_translator(profile[:bodytype])
  end

  def self.sexuality_criteria_met?(profile, gender_obj)
    gender_array = gender_obj[:sexualities][profile[:gender]]
    !gender_array.nil? && gender_array.include?(profile[:sexuality])
  end

  def self.change_name(old_name, new_name)
    new_matches = Match.where(name: new_name)
    if new_matches.size.zero?
      changes = Match.where(name: old_name).update_all(name: new_name)
    else
      changes, old_matches = 0, Match.where(name: old_name)
      old_matches.each do |match|
        changes += 1
        new_match = Match.find_by(account: match.account, name: new_name)
        new_match.increment!(:counts, by=match.counts)
        new_match.save!
        match.delete
      end
    end
    UsernameChange.create(old_name: old_name, new_name: new_name) unless changes.zero?
  end

  def self.ignore(account, handle)
    Match.where(account: account, name: handle).update_all(ignored: true)
  end

  def self.add(details, gender, extra)
    response = false
    begin
      Match.create(details) do |c|
        c.gender = gender
        extra.each do |key, value|
          c[key.to_sym] = value
        end
        response = true
      end
    rescue
      response = false
    end
    return response
  end

  def self.should_ignore?(profile, id)
    false
  end
end
