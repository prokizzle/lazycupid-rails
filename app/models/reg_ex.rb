class RegEx

  def self.parsed_location(string)
    result    = string.split(",")
    if result.size == 3
      city, state, country = result[0], result[1], result[2]
    elsif result.size == 2
      begin
        city, state, country = result[0], result[1], "United States"
      rescue
        city, state, country = "", "", ""
      end
    end
    {city: city, state: state, country: country }
  end


end
