class Card

  def self.find_or_create(customer, token)
    if exists_for? customer
      card = customer.default_card
    else
      card = customer.cards.create(card: token)
      customer.default_card = card.id
      customer.save
    end
    p customer
    return customer.default_card
  end

  def self.exists_for?(customer)
    begin
      customer.default_card
      result = true
    rescue
      result = false
    end
    return result
  end

  def self.retrieve(customer, card_id)
  end

  def self.authorize(user, token)

    customer = Billing.customer(user)
    # delete_all_cards(customer)
    card, error, message = nil, false, nil
    begin
      card = customer.cards.create(card: token)
      # p address
      # card.address_line1 = address[:address_line1]
      # card.address_line2 = address[:address_line2]
      # card.address_city = address[:address_city]
      # card.address_state = address[:address_state]
      # card.address_zip = address[:address_zip]
      # card.address_country = address[:address_country]
      # card.save
    rescue Exception => e
      error = true
      message = e.message
    end
    return {card: card, error: error, message: message}
  end

  # def self.delete_all_cards(customer)
  #   customer.cards.all.each { |card| card.delete }
  # end


end
