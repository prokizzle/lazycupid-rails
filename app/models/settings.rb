class Settings
  def initialize(id)
    @user = User.find_by(id: id)
  end

  def distance
    @user.settings(:match_settings).max_distance
  end

  def distance=(distance)
    @user.settings(:match_settings).max_distance = distance.to_i
    @user.save
  end

end
