class Roller
  def self.initialize
    # @queue = Match.queue
  end

  # def self.next_match
  #   if $queue.empty?
  #     $queue = Match.queue.to_a.reverse
  #   end
  #   return $queue.shift["name"]
  # end

  def self.visit(username)
    Profile.parse(username, current_user.id)
  end

  def self.enabled?(id)
    enabled = $redis.hget("roll_enabled", id)
    if enabled.nil?
      begin
        enabled = User.find_by(id: id).settings(:roller).status.to_s
        $redis.hset("roll_enabled", id, enabled)
      rescue
      end
    end
    return enabled == "true"
  end

  def self.disabled?(id)
    !enabled?(id)
  end

  def self.enable!(id)
    $redis.hset("roll_enabled", id, true)
  end

  def self.pause!
    $redis.set("paused", true)
  end

  def self.resume!
    $redis.set("paused", false)
  end

  def self.paused?
    $redis.get("paused") == "true"
  end

  def self.restart!(id)
    disable!(id)
    enable!(id)
  end

  def self.session(id)
    user = User.find_by(id: id)
    user.settings(:roller).session
  end

  def self.clear_session(id)
    user = User.find_by(id: id)
    user.settings(:roller).session = nil
    user.save
  end

  # def self.session = (args)
  #   user = User.find_by(id: args[:id])
  #   user.settings(:roller).session = args[:session]
  #   user.save
  # end

  def self.disable!(id)
    # Sidekiq::Queue.new.clear
    StopAllJobsWorker.perform_async(id)
    user = User.find_by(id: id)
    Roller.queue = {id: id, queue: []}
    $redis.hset("roll_enabled", id, false)
    user.settings(:roller).status = false
    user.save
  end

  def self.queue=(obj)
    id = obj[:id]
    queue = obj[:queue]
    if $redis.llen("#{id}_queue") == 0
      $redis.rpush("#{id}_queue", queue) if queue.size > 0
    end
  end

  def self.queue_size(id)
    return $redis.llen("#{id}_queue")
  end

  def self.next(id)
    return $redis.rpop("#{id}_queue")
  end

  def self.visits(id)
    visits = $redis.get("#{id}_visits").to_i
    if visits.nil?
      visits = 0
    end
    visits += 1
    $redis.set("#{id.to_s}_visits", visits.to_s)
    return visits.to_i
  end

  def self.live(id)
    print 0
    loop do
      size = Roller.queue_size(id)
      spaces = size < 10 ? " " : "  "
      print "\rQueue Size: #{size}"
      sleep 1
    end
  end




  def self.restart_enabled!
    User.find_each do |u|
      Roller.enable!(u.id) if u.settings(:roller).status
    end
  end

  def self.disable_all!
    User.find_each do |u|
      Roller.disable!(u.id) if u.settings(:roller).status
    end
  end

  def self.disable_all_users!
    User.find_each do |u|
      u.settings(:roller).status = false
      u.save
    end
  end

  def self.clear_all_queues!
    User.find_each do |u|
      $redis.del("#{u.id}_queue")
    end
  end

  def self.clear(id)
    $redis.del("#{id}_queue")
  end

  def self.should_roll_now?(user)
    Roller.enabled?(user.id) && Time.now.in_time_zone(user.timezone).hour.between?(6,22)
  end

  def self.push(match, record)

    # Pusher.url = "http://aca77e9f5a07994fdda1:1e8a1e5549c177c051ac@api.pusherapp.com/apps/89450"

    # Pusher['test_channel'].trigger('my_event', {
    #                                  match: match,
    #                                  record: record
    # })
  end

  def self.push_status(id)
    gon.push({user_id: current_user.id})
    RealtimeMessage.publish(id, 'roll_status', {status: Roller.enabled?(id), queueSize: Roller.queue_size(id)})
  end
end
