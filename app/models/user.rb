class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable
  has_settings do |s|
    s.key :personal, :defaults => { :age => 25, :sexuality => 'straight', :gender => "M", useragent: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.104 Safari/537.36" }
    s.key :match_settings,  :defaults => { :max_distance => 50, :min_age => 18, :max_age => 45, min_match_percent: 60, min_days_ago: 3, max_visits: 10, body_small: true, body_medium: true, body_large: false, creep_factor: 3}
    s.key :roller, :defaults => {:status => false, :session => nil}
    s.key :role, :defaults => {:admin => false}
    s.key :sessions, :defaults => {:mechanize => nil, :headless => nil}
  end

  def account
    self.settings(:personal).account
  end

  def timezone
    # self.settings(:personal).timezone
    return "Central Time (US & Canada)"
  end
end
