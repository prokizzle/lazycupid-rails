class Session

  def self.create(id, params)
    user = User.find_by(id: id)
    if OkcupidSessions.mechanize(id).nil?
      browser = Browser.new(username: params[:username], password: params[:password])
      browser.login
    else
      browser = Browser.new(session: OkcupidSessions.mechanize(id), user_agent: user.settings(:personal).useragent)
    end
    if browser.is_logged_in?
      user.settings(:personal).account = params[:username]
      user.settings(:sessions).mechanize = browser.save_session
      user.save
    end
    return browser.is_logged_in?
  end

  def self.create_headless(id, params)
    if OkcupidSessions.headless(id).nil? || OkcupidSessions.headless(id).empty?
      headless = HeadlessBrowser.new(username: params[:username], password: params[:password])
    else
      headless = HeadlessBrowser.new(session: OkcupidSessions.headless(id))
    end
    if headless.is_logged_in?
      user = User.find_by(id: id)
      user.settings(:sessions).headless = headless.save_session
      user.save
    end
    return headless.is_logged_in?
  end
end
