class OutgoingVisit < ActiveRecord::Base
  scope :by_month, lambda { |month| where('extract(month from created_at) = ?', month).where('extract(year from created_at) = 2014') }
end
