class Preference < ActiveRecord::Base
  belongs_to :user

  validates_uniqueness_of :name, :scope => :user_id

  def self.get_prefs(id)
    user = User.find_by(id: id)

    return {
      subscribed: subscribed?(user),
      my_gender: user.settings(:personal).gender,
      my_age: user.settings(:personal).age,
      my_sexuality: user.settings(:personal).sexuality,
      max_distance: user.settings(:match_settings).max_distance,
      min_match_percent: user.settings(:match_settings).min_match_percent,
      max_age: user.settings(:match_settings).max_age,
      min_age: user.settings(:match_settings).min_age,
      creep_factor: user.settings(:match_settings).creep_factor,
      enabled: Roller.enabled?(user.id),
      body_large: user.settings(:match_settings).body_large == "true",
      body_medium: user.settings(:match_settings).body_medium == "true",
      body_small: user.settings(:match_settings).body_small == "true",
      session: !user.settings(:sessions).headless.nil? && !user.settings(:sessions).mechanize.nil?

    }
  end

  def self.update_prefs(id, params)
    user = User.find_by(id: id)
    user.settings(:personal).age = params[:my_age]  if params[:my_age]
    user.settings(:personal).sexuality = params[:my_sexuality] if params[:my_sexuality]
    user.settings(:personal).gender = params[:my_gender] if params[:my_gender]
    user.settings(:match_settings).max_distance = params[:max_distance] if params[:max_distance]
    user.settings(:match_settings).min_age = params[:min_age] if params[:min_age]
    user.settings(:match_settings).max_age = params[:max_age] if params[:max_age]
    user.settings(:match_settings).body_large = params[:body_large] if !params[:body_large].nil?
    user.settings(:match_settings).body_medium = params[:body_medium] if !params[:body_medium].nil?
    user.settings(:match_settings).body_small = params[:body_small] if !params[:body_small].nil?
    if params[:enabled]
      if subscribed?(user) && logged_in?(user)
        Roller.enable!(user.id) unless Roller.enabled?(user.id)
      else
        Roller.disable!(user.id) if Roller.enabled?(user.id)
      end
    elsif !params[:enabled].nil?
      Roller.disable!(user.id) unless Roller.disabled?(user.id)
    end

    user.save
    Match.creep_factor(id, params[:creep_factor]) if !params[:creep_factor].nil?
  end

  def self.get_body_size_prefs(id)

  end

  def self.subscribed?(user)
    if $rollout.active?(:billing, user)
      return user.subscribed == true
    else
      return true
    end
  end

  def self.logged_in?(user)
    return !user.settings(:roller).session.nil?
  end

  def self.set_body_size_prefs(id, size_prefs)
    user.settings(:match_settings).body_large = size_prefs[:large]
    user.settings(:match_settings).body_medium = size_prefs[:medium]
    user.settings(:match_settings).body_small = size_prefs[:small]
  end

  private

  def preference_params
    params.require(:preference).permit(:name, :value)
  end
end
