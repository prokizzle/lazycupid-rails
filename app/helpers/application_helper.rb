module ApplicationHelper
  def roller_switch_state(id)
    Roller.enabled?(id) ? "checked" : ""
  end

  def roller_enabled?
    Roller.disabled?(current_user.id)
  end

  def total_visits
    OutgoingVisit.where(account: current_user.settings(:personal).account).size
  end

  def total_matches
    Match.where(account: current_user.settings(:personal).account).size
  end

  def total_messages
    IncomingMessage.where(account: current_user.settings(:personal).account).size
  end

  # def user_id
  #   current_user.id
  # end
end
