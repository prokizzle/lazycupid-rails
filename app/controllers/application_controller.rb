class ApplicationController < ActionController::Base
  respond_to :html, :json
  # include LazyCupid
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def index
  end

  def okcupid_account_name
    @user ||= User.find_by(id: current_user.id)
    @user.settings(:personal).account
  end

  protected

  def verified_request?
    super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
  end

end
