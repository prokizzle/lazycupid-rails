class MatchesController < ApplicationController
  def index
  end

  def more_matches
    MatchPageScraperWorker.perform_async(current_user.id, current_user.settings(:personal).account)
    render json: {}.to_json
  end

  def stats
    render json: {outgoing: OutgoingVisit.count, incoming: IncomingVisit.count, messages: IncomingMessage.count, users: User.count}
  end

  def visit
    link = "http://okcupid.com/profile/#{params[:id]}"
    render json: Profile.parse(params[:id], current_user.id)
  end

  def name_change
    render json: UsernameChange.random.as_json
  end

  def name_changes
    gon.push({
               :user_id => current_user.id
    })
  end



  def ignore
    user = User.find_by(id: 1)
    match = Match.find_by(account: user.settings(:personal).account, name: params[:name]).update(ignored: true)
    render json: {status: match}.to_json
  end

  def show
    account = current_user.settings(:personal).account
    @match = Match.find_by(account: account, name: params[:id])
    @outgoing_visits = OutgoingVisit.where(account: account, name: params[:id])
    @messages_received = IncomingMessage.where(account: account, username: params[:id]).size
  end
end
