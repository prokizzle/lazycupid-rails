class PreferencesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_user_id

  def index
    @pref ||= {}
    @pref[:my_age] = current_user.settings(:personal).age
  end

  def get_preferences
    render json: Preference.get_prefs(current_user.id).to_json
  end

  def update
    Preference.update_prefs(current_user.id, params)
    render json: Preference.get_prefs(current_user.id).to_json
  end

  private

  def set_user_id
    gon.push({user_id: current_user.id})
  end

end
