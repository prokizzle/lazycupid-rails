class BillingController < ApplicationController
  before_filter :authenticate_user!

  def add_card
    p current_user
    render json: Card.authorize(current_user, params[:token]).as_json
  end

  def create_subscription
    render json: Subscription.create(current_user).as_json
  end

end
