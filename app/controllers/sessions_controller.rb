class SessionsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_user_id


  def new
    # raise error
    current_user.settings(:personal).useragent = request.env["HTTP_USER_AGENT"]
    unless current_user.settings(:roller).session.nil?
      redirect_to '/roller'
    end
  end

  def destroy
    user = User.find_by(id: current_user.id)
    user.settings(:roller).session = nil
    user.save
    reset_session
    # unless session[:browser].nil?
    #   browser = YAML::load(session[:browser])
    #   browser.logout
    #   session[:account] = nil
    #   session[:browser] = nil
    # end
    RealtimeMessage.publish(current_user.id, 'redirect', {page: '/roll'})

  end

  def create
    id = current_user.id
    logged_in = Session.create_headless(id, params)
    if logged_in
      logged_in = Session.create(id, params)
    end
    render json: {logged_in: logged_in}.to_json
  end

  private
  def set_user_id
    gon.push({user_id: current_user.id})
  end
end
