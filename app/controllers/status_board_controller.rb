class StatusBoardController < ApplicationController
  def user_statuses
    render text: StatusBoard.user_statuses
  end

  def visits
    render text: StatusBoard.outgoing_visits
  end

  def messages
    render text: StatusBoard.incoming_messages
  end

  def queues
    render text: StatusBoard.sidekiq
  end

  def conversions
    render text: StatusBoard.conversions
  end

  def processes
    render text: StatusBoard.processes
  end

  def distances
    render text: StatusBoard.distances
  end

  def stats
    render json: StatusBoard.statuses
  end

  def roller_queues
    render text: StatusBoard.roller_queues
  end
end