class UsersController < ApplicationController
  before_filter :authenticate_user!

  def billing
    render json: Billing.customer(current_user)
  end

  def account_name
    render json: {account: Account.name(current_user.id)}.as_json
  end
end
