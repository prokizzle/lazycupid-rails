class StatsController < ApplicationController
  def messages_by_month
    account = current_user.settings(:personal).account
    render json: IncomingMessage.where(account: account).group("DATE_TRUNC('month', created_at)").count.as_json
  end

  def visits_by_month
    account = current_user.settings(:personal).account
    render json: OutgoingVisit.where(account: account).group("DATE_TRUNC('month', created_at)").count.as_json
  end
end
