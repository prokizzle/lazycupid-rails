class NextActionController < ApplicationController
  before_filter :authenticate_user!

  def index
  end

  def next
    render json: InboxParser.next_message(current_user.id, "msg_queue", "cache_read_messages").as_json
  end

  def new
    render json: InboxParser.next_message(current_user.id, "new_msg_queue", "cache_unread_messages").as_json
  end

  def mutual
    render json: InboxParser.next_message(current_user.id, "mutual_matches", "cache_mutual_match_messages").as_json
  end

  def all
    render json: InboxParser.next_message(current_user.id, "all_msg_queue", "cache_all_messages").as_json
  end
end
