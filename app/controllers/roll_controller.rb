require 'sidekiq/api'

class RollController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_user_id
  # def roll
  #   $queue ||= []
  #   if $queue.empty?
  #     $queue = Match.queue.to_a
  #   end
  #   render json: $queue.shift.to_json
  # end

  def activity_feed
    current_user.settings(:personal).useragent = request.env["HTTP_USER_AGENT"]
  end

  def continuous_roll
    Roller.enable!(current_user.id)
    render json: {status: Roller.enabled?(current_user.id)}.to_json
  end

  def roll_status
    # render json: {status: Roller.enabled?}.to_json
    render json: {status: Roller.enabled?(current_user.id)}.to_json
    # push_roll_status
  end

  def stop_rolling
    Roller.disable!(current_user.id)
    push_roll_status
    render json: {status: Roller.enabled?(1)}.to_json
  end

  def queue_size
    render json: {queueSize: Roller.queue_size(current_user.id), visited: OutgoingVisit.where(account: current_user.settings(:personal).account).size}
  end

  def visit
    VisitorWorker.perform_async(params[:id], session[:account], current_user.id)
  end

  def delete_user
    match = Match.where(account: "primal_objective", name: params[:username])
    match.inactive = true
    match.save
  end

  def get_user_id
    render json: {id: current_user.id}
  end

  def session_status
    render json: {authorized: !Roller.session(current_user.id).nil?}.as_json
  end

  private
  def set_user_id
    gon.push({user_id: current_user.id})
  end
end
