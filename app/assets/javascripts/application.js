
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require foundation
//= require turbolinks
//= require react
//= require react_ujs
//= require components
//= require lodash
//= require alertify
//= require moment
//= require sweetalert
//= require cupcake-react-modal
//= require spinjs
//= require_tree .
