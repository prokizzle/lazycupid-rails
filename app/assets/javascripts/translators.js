
var types = {
      "A little extra": "large",
      "Average": "medium",
      "Curvy": "medium",
      "Full figured": "large",
      "Voluptuous": "large",
      "Overweight": "large",
      "Thin": "small",
      "Fit": "small",
      "Athletic": "small",
      "Skinny": "small",
      "Rather not say": "large",
      "Jacked": "medium",
      "Used up": "small"
    };

    var bodyTypeTranslator = function(bodyType) {
      if (bodyType === '—') {
            return "unknown";
      } else {
            return types[bodyType];
      }
    }

    var genderPronounTranslator = function(gender) {
      if (gender === "F") {
        return "she"
      } else {
        return "he"
      }
    }

    var genderHimOrHer = function(gender) {
      if (gender === "F") {
        return "her"
      } else {
        return "him"
      }
    }
