var SettingsSelectMenu = React.createClass({
  handleChange: function(e) {
    this.props.handleChange({
      prefName: this.props.prefName,
      prefVal: e.target.value
    });
  },
  render: function(){
    var self = this;
    var options = this.props.options.map(function(option, i){
      return (
          <option value={option} className='personal'>{self.props.labels[i]}</option>
      );
    });

    var label = this.props.prefName
      .replace('my_', '')
      .replace('_', ' ');

    return (
      <div className="row">
          <div className="columns large-6 small-12 medium-6">
              <div className="row">
                  <div className="columns large-3">
                    <label htmlFor={this.props.prefName}>{label}</label>
                  </div>
                  <div className="columns large-9">
                      <select name={this.props.prefName} value={this.props.value} onChange={this.handleChange}>
                          {options}
                      </select>
                  </div>
              </div>
          </div>
      </div>
    );
  }
});