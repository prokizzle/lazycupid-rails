var MatchSettings = React.createClass({

  render: function(){
    return (
        <div>
          <h3>About Your Match</h3>
          <SettingsInputField
            prefName='max_distance'
            value={this.props.maxDistance}
            handleChange={this.props.handleChange}
          />

          <SettingsInputField
            prefName='min_match_percent'
            value={this.props.minMatchPercent}
            handleChange={this.props.handleChange}
          />

          <SettingsInputField
            prefName='min_age'
            value={this.props.minAge}
            handleChange={this.props.handleChange}
          />

          <SettingsInputField
            prefName='max_age'
            value={this.props.maxAge}
            handleChange={this.props.handleChange}
          />

          <SettingsSelectMenu
            options={[1,2,3,4,5]}
            labels={['Shy', 'Normal', 'Intrigued', 'Aggressive', 'Desperate']}
            value={this.props.creepFactor}
            prefName='creep_factor'
            handleChange={this.props.handleChange}
          />

          <SettingsSelectMenu
            prefName='body_large'
            options={['true', 'false']}
            labels={['yes', 'no']}
            value={this.props.bodyLarge}
            handleChange={this.props.handleChange}
          />

          <SettingsSelectMenu
            prefName='body_medium'
            options={['true', 'false']}
            labels={['yes', 'no']}
            value={this.props.bodyMedium}
            handleChange={this.props.handleChange}
          />

          <SettingsSelectMenu
            prefName='body_small'
            options={['true', 'false']}
            labels={['yes', 'no']}
            value={this.props.bodySmall}
            handleChange={this.props.handleChange}
          />

        </div>


    );

  }
})