var NextMessageButton = React.createClass({
  getInitialState: function(){
    return {
      nextMessage: null,
      reminaing: 0
    }
  },
  componentDidMount: function(){
    this.getNextMessage();
  },
  getNextMessage: function(){
    var self = this;
    $.get(self.props.url, function(data){
      if (data.remaining == 0) {
        setTimeout(self.handleClick, 5000);
      };

      self.setState({
        nextMessage: data.nextMsg,
        remaining: data.remaining
      });
    });
  },
  handleClick: function(e){
    if (e)
      e.preventDefault();

    var self = this;
    if (this.state.nextMessage)
      open('http://www.okcupid.com' + self.state.nextMessage, '_blank');
      self.setState({nextMessage: null})
    self.getNextMessage();
  },
  render: function(){
    return(
      <button onClick={this.handleClick}>
        {this.props.btnLabel}
      </button>
    );
  }
});