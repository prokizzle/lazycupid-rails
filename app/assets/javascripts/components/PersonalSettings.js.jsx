var PersonalSettings = React.createClass({
  render: function() {
    return(
      <div>
        <SettingsSelectMenu
          prefName='enabled'
          value={this.props.enabled}
          options={['true', 'false']}
          labels={['enabled', 'disabled']}
          handleChange={this.props.handleChange}
        />

        <h3>About You</h3>

        <SettingsInputField
          prefName='my_age'
          value={this.props.age}
          handleChange={this.props.handleChange}
        />
        <SettingsSelectMenu
          options={['gay', 'straight', 'bisexual']}
          labels={['Gay', 'Straight', 'Bisexual']}
          value={this.props.sexuality}
          handleChange={this.props.handleChange}
          prefName='my_sexuality'
        />
        <SettingsSelectMenu
          options={['M', 'F']}
          labels={['Male', 'Female']}
          value={this.props.gender}
          handleChange={this.props.handleChange}
          prefName='my_gender'
        />
      </div>
    );
  }
});