var SettingsBooleanSwitch = React.createClass({
  handleChange: function(e) {
    this.props.handleChange({
      prefName: this.props.prefName,
      prefVal: e.target.value
    });
  },
  render: function() {
    var label = this.props.prefName.replace('_', ' ');
    return(
      <div className="row">
          <div className="columns large-3 small-12 medium-3 small-offset-4 large-offset-0 end">
              <strong>{label}</strong>
              <div className="switch">
                  <input type="checkbox" checked={this.props.value} onChange={this.handleChange} />
                  <label htmlFor={this.props.prefName}></label>
              </div>
          </div>
      </div>
    );
  }
});