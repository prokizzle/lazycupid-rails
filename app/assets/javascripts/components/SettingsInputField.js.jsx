var SettingsInputField = React.createClass({
  handleChange: function(e) {
    this.props.handleChange({
      prefName: this.props.prefName,
      prefVal: e.target.value
    });
  },
  render: function(){
    var label = this.props.prefName
      .replace('my_', '')
      .replace('_', ' ');
    return(
      <div className="row">
          <div className="columns large-6 small-12 medium-6">
              <div className="row">
                  <div className="columns large-3">
                      <label htmlFor={this.props.prefName}>{label}</label>
                  </div>
                  <div className="columns large-9 small-4 end">
                      <input type="text" value={this.props.value} onChange={this.handleChange} name={this.props.prefName}/>
                  </div>
              </div>
          </div>
      </div>
    );
  }
});