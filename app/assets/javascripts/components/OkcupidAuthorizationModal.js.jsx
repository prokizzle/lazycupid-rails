var OkcupidAuthorizationModal = React.createClass({
  getInitialState: function(){
    return {
      session: true,
      loginMessage: '',
      username: '',
      password: ''
    }
  },
  componentDidMount: function(){
    var self = this;
    $.get('/api/get-preferences', function(prefs){
      self.setState(prefs);
    });
  },
  handleUsernameChange: function(e){
    this.setState({
      username: e.target.value
    });
  },
  handlePasswordChange: function(e) {
    this.setState({
      password: e.target.value
    });
  },
  handleLogin: function(e) {
    var self = this;
    e.preventDefault();
    self.setState({
      loginMessage: 'Logging in...'
    });

    $.post('/createSession', {
      username: self.state.username,
      password: self.state.password
    }, function(data){
      if (data.logged_in) {
        self.setState({
          session: true,
          loginMessage: 'Successfully logged in!',
          username: '',
          password: ''
        });
      } else {
        self.setState({
          loginMessage: 'Incorrect username or password',
          password: ''
        });
      }
    });
  },
  render: function(){
    return(
      <Modal
        visible={!this.state.session} >
        <div className="loginBox panel">
            <div className="row">
                <div className="large-8 medium-4 columns">
                    <h2>Authenticate with OKCupid</h2>
                    <p>LazyCupid does NOT:</p>
                    <ul>
                        <li>Save your login information</li>
                        <li>Store any of your personal information</li>
                    </ul>
                    <div className="row">
                        <div className="columns large-6">
                            <p>{this.state.loginMessage}</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="large-4 columns">
                            <label>Username
                                <input type="text" name="okcUser" onChange={this.handleUsernameChange} />
                                <br/>
                            </label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="large-4 columns">
                            <label>Password
                                <input type="password" name="okcPass" onChange={this.handlePasswordChange} />
                                <br/>
                            </label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="large-2 columns">
                            <button className="button small" id="login" onClick={this.handleLogin}>Login</button>
                        </div>
                    </div>
                </div>
                <div className="large-4 medium-4 columns">
                    <img src="/assets/lazycupid_logo.png" height="60" width="60" />
                </div>
            </div>
        </div>

      </Modal>
    );
  }
});