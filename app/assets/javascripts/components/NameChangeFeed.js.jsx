var NameChangeFeed = React.createClass({
  getInitialState: function(){
    return {nameChanges: new FixedQueue(10, [])}
  },
  componentDidMount: function(){
    this.pollForNameChanges();
  },
  pollForNameChanges: function(){
    var self = this;
    setTimeout(function(){
        var changes = self.state.nameChanges;
      $.get('/api/random-name-change', function(data){
        changes.unshift(data);
        self.setState({
          nameChanges: changes
        });
        console.log(self.state.nameChanges);
        self.pollForNameChanges();
      });
    }, 5000);
  },
  render: function(){
    if (this.state.nameChanges.length) {
      var feedItems = this.state.nameChanges.map(function(item){
        console.log(JSON.stringify(item));
        var newNames = item.new_names.map(function(name){
          return ' became ' + name;
        });
        return (<div className='panal'><h3>{item.old_name}{newNames}</h3></div>);
      });
    } else {
      var feedItems = <h3>Loading...</h3>;
    };

    return (
      <div>
        {feedItems}
      </div>
    );
  }
});