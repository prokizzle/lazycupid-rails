var PreferencePane = React.createClass({
  getInitialState: function(){
    return {
      enabled: false,
      my_age: 0,
      my_gender: 'M',
      my_sexuality: 'straight',
      max_distance: 0,
      min_match_percent: 0,
      min_age: 0,
      max_age: 0,
      creep_factor: 5,
      body_large: true,
      body_medium: true,
      body_small: true
    };
  },
  componentDidMount: function(){
    var self = this;
    $.get('/api/get-preferences', function(prefs){
      console.log(JSON.stringify(prefs))
      self.replaceState(prefs);
    });
  },
  handleSwitchClick: function(e) {
    var self = this;
    this.setState({enabled: !self.state.enabled});
  },
  handleSubmit: function(e){
    var self = this;
    var spinner = new Spinner().spin();
    document.getElementsByTagName('body')[0].appendChild(spinner.el);
    $.post('/api/update-preferences', self.state, function(prefs){
        self.setState(prefs);
        console.log(JSON.stringify(prefs));
        spinner.stop();
      });
  },
  handleSettingsChange: function(e) {
    var prefs = this.state;
    prefs[e.prefName] = e.prefVal;
    this.setState(prefs);
  },
  render: function(){
    return (
            <div className="panel">

              <PersonalSettings
                age={this.state.my_age}
                gender={this.state.my_gender}
                sexuality={this.state.my_sexuality}
                handleChange={this.handleSettingsChange}
                enabled={this.state.enabled}
              />

              <MatchSettings
                maxDistance={this.state.max_distance}
                minMatchPercent={this.state.min_match_percent}
                minAge={this.state.min_age}
                maxAge={this.state.max_age}
                creepFactor={this.state.creep_factor}
                bodyLarge={this.state.body_large}
                bodyMedium={this.state.body_medium}
                bodySmall={this.state.body_small}
                handleChange={this.handleSettingsChange}
              />

              <button id='updatePreferences' onClick={this.handleSubmit}>Save</button>

            </div>
    );
  }
});