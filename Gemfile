# ruby "2.1.5"

source 'https://rubygems.org'

# core
gem 'rails', '4.1.9'
gem 'puma'
gem 'redis-rails'
gem 'rack-cache'
gem "mechanize"
gem "watir-webdriver"
gem "watir-scroll"
gem "webdriver-user-agent"
gem 'nikkou'
gem 'ledermann-rails-settings'
gem 'react-rails', '~> 1.0'

gem 'rails_12factor', group: :production
gem 'newrelic_rpm'
gem 'health_check'

# parsing tools
gem "chronic"
gem "time-lord"
gem "uuidtools"
gem "lingua"
gem 'ruby-units'
gem 'panic_board_data'

gem 'aws-sdk', "~> 1.0"

# database
gem 'pg'
gem 'seed_dump'
gem 'groupdate'
gem 'paperclip'
gem 'figaro'
gem 'paperclip-dropbox', ">= 1.1.7"

# scheduling
gem "sidekiq"
# gem 'sidekiq_monitor'
# gem 'sidekiq-unique-jobs'
# gem 'sidekiq-throttler'
gem 'sidekiq-rate-limiter', :require => 'sidekiq-rate-limiter/server'
gem 'sidekiq-runner'
gem 'sidetiq'
gem 'sidekiq-failures'

# billing
gem 'stripe'
gem 'rollout'


# styling
gem 'sass-rails'
gem 'uglifier', '>= 1.3.0'
# gem 'therubyracer',  platforms: :ruby
gem 'foundation-rails', '~> 5.4'

# authentication
gem 'devise'
gem 'bcrypt', '~> 3.1.7'


# javascript
gem 'jquery-rails'
gem 'turbolinks'
gem 'lodash-rails'
gem 'ejs'
gem 'phantomjs'

# bower
source 'https://rails-assets.org' do
  gem 'rails-assets-spinjs'
  gem 'rails-assets-alertify'
  gem 'rails-assets-moment'
  gem 'rails-assets-restangular'
  gem 'rails-assets-sweetalert'
  gem 'rails-assets-cupcake-react-modal'
end

# serialization
gem 'msgpack'

# realtime
gem 'faye'
gem 'gon'
gem 'sinatra'
# gem 'pusher'

# error handling
# gem "airbrake"
# gem 'honeybadger'

# visualization
gem "highline"


# gem "pusher"
# gem "httparty"

group :development do
  gem 'better_errors'
  gem 'bower-rails'
  # gem 'debugger'
  gem "quiet_assets"
  gem "progress_bar"
  gem 'spring'

  gem 'binding_of_caller'
  gem "bloat_check"
  gem "chromedriver-helper"
  gem 'rerun'
  gem 'foreman'
  gem 'ops_tasks'
end

group :test do
  gem 'cucumber-rails', :require => false
end
gem 'sdoc', '~> 0.4.0',          group: :doc
