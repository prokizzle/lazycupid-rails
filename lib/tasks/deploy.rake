namespace :sidekiq do
  task :deploy => :environment do
    client = AWS::OpsWorks::Client.new
    client.create_deployment(
      :stack_id => ENV['aws_stack_id'],
      :instance_ids => [ENV['aws_sidekiq_instance_id']],
      # :app_id => ENV['aws_app_id'],
      :command => {
        name: "execute_recipes",
        args: {"recipes" => [ENV['aws_deploy_recipe']]}
      }
    )
    %x{RAILS_ENV=production newrelic deployments}
  end
end

namespace :portal do
  task :deploy => :environment do
    client = AWS::OpsWorks::Client.new
    client.create_deployment(
      :stack_id => ENV['aws_stack_id'],
      :instance_ids => [ENV['aws_portal_instance_id']],
      # :app_id => ENV['aws_app_id'],
      :command => {
        name: "execute_recipes",
        args: {"recipes" => [ENV['aws_portal_deploy_recipe']]}
      }
    )
    %x{RAILS_ENV=production newrelic deployments}
  end

  task :nginx => :environment do
    client = AWS::OpsWorks::Client.new
    client.create_deployment(
      :stack_id => ENV['aws_stack_id'],
      :instance_ids => [ENV['aws_portal_instance_id']],
      # :app_id => ENV['aws_app_id'],
      :command => {
        name: "execute_recipes",
        args: {"recipes" => ["lazycupid::setup-nginx"]}
      }
    )
    %x{RAILS_ENV=production newrelic deployments}
  end
end

task :update_cookbooks => :environment do
  client = AWS::OpsWorks::Client.new
  client.create_deployment(
    :stack_id => ENV['aws_stack_id'],
    :instance_ids => JSON.parse(ENV['aws_instance_ids']),
    # :app_id => ENV['aws_app_id'],
    :command => {
      name: "update_custom_cookbooks"
    }
  )
end
