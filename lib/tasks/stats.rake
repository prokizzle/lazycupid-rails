namespace :stats do
  task :visits_to_message_ratio => :environment do
    counts = []
    msgs = IncomingMessage.all
    msgs.each do |msg|
      Match.where(name: msg.username, account: msg.account).find_each do |m|
        counts.push m.counts
      end
    end
    puts "#{counts.instance_eval { reduce(:+) / size.to_f }} visits before message"
  end
end
