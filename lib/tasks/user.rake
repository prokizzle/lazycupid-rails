task :change_password => :environment do
  id = ask("id: ")
  password = ask("password: ")
  encrypted_password = ::BCrypt::Password.create(password)
  user = User.find_by(id: id)
  user.encrypted_password = encrypted_password
  user.save
end