task :fix_sexuality => :environment do
  Match.select(:sexuality).distinct.map{|s| s.sexuality}.each do |sexuality|
    unless (sexuality =~ /,/).nil?
      Match.where(sexuality: sexuality).update_all(sexuality: sexuality.split(",").first)
    end
  end
end

task :fix_body_type => :environment do
  Match.select(:body_type).distinct.map{|m| m.body_type}.each do |body_type|
    unless ["small", "medium", "large"].include?(body_type)
      new_body = Match.body_type_translator(body_type)
      Match.where(body_type: body_type).update_all(body_type: new_body)
    end
  end
end

task :test_profile => :environment do
  name = "karlahoney"
  user_id = 7
  link = "http://www.okcupid.com/profile/#{name}"
  uuid = UUIDTools::UUID.random_create
  begin
    user                  = User.find_by(id: user_id)
    cookies               = user.settings(:roller).session
    agent = Mechanize.new do |a|
      a.ssl_version = :TLSv1
    end
    agent.cookie_jar      = YAML::load(cookies)
    agent.user_agent      = user.settings(:personal).useragent
    url                   = URI.escape(link)
    agent.read_timeout    = 30
    page_object           = agent.get(link)
    page_object.encoding  = 'utf-8'
    # page_body             = page_object.parser.xpath("//body").to_html
    page_source           = page_object.parser.xpath("//html").to_s
    result_object        = {url: link.to_s, ready: true, source: page_source.to_s, retrieved: false, inactive: false}
  rescue Exception => e
    result_object = {url: link, inactive: true, ready: true, retrieved: false, error: e.message}
  end

  result = result_object

  # profile = Profile.parse(result)
end
