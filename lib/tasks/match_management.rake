task :ignore_inbox_users => :environment do
  IncomingMessage.all.each do |message|
    IgnoreMatchWorker.perform_async(message.account, message.username)
  end
end