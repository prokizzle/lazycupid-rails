task :best do
  best = Hash.new(0)
  states = Match.where(account: 'atx_male_unicorn', match_percent: 90..99).select(:state).distinct
  states.each do |state|
    best[state.state] = Match.where(account: 'atx_male_unicorn', state: state.state, match_percent: 90..99).size
  end
  best.sort_by(&:last)
end

def best(id)
  user = User.find_by(id: id)
  account = user.settings(:personal).account
  best = Hash.new(0)
  cities = Match.where(account: account, match_percent: 80..99).select(:city).distinct
  cities.each do |city|
    best[city.city] = Match.where(account: account, city: city.city, match_percent: 90..99).size
  end
  return best.sort_by(&:last)
end
