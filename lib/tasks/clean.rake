require 'highline/import'
require 'progress_bar'
task :delete_account => :environment do
  account = ask("account: ")
  # matches = Match.where(account: account)
  # bar = ProgressBar.new(matches.size)
  # Match.where(account: account).find_in_batches do |group|
  #   # group.each{|person| DeleteMatchWorker.perform_async(person, account)}
  #   # bar.increment!
  # end
  DeleteMatchWorker.perform_async(account)
end

task :clean_duplicates => :environment do
  Match.where(account: "primal_objective").find_in_batches do |group|
    group.each do|person|
      puts "#{person.name} #{person.account}"
      DuplicateDeletionWorker.perform_async(person.name, person.account)
    end
  end
end

task :record_count => :environment do
  account = ask("account: ")
  puts "#{Match.where(account: account).size} matches"
end

task :check_progress => :environment do
  previous_size = Sidekiq::Queue.new.size
  bar = ProgressBar.new(previous_size, :eta)
  puts "Time remaining: "
  1000.times do
    unless Sidekiq::Queue.new.size == 0
      new_size = Sidekiq::Queue.new.size
      bar.increment! (previous_size - new_size)
      previous_size = Sidekiq::Queue.new.size
    end
  end
end

task :dupes => :environment do
  account = ask("account: ")
  total_records = Match.where(account: account).size
  unique_users = Match.where(account: account).select(:name).distinct.size
  accurate = total_records == unique_users + 1
  puts accurate ? "All is OK" : "#{total_records - unique_users} duplicates found. Searching..."
  unless accurate
    Match.select(:account, :name).group(:account, :name).having("count(*) > 1").each do |m|
      Match.delete_duplicate(m.name, m.account)
      puts "#{m.account}: #{m.name}"
    end
  end
end

task :message_dupes => :environment do
  account = ask("account: ")
  total_records = IncomingMessage.where(account: account).size
  unique_users = IncomingMessage.where(account: account).select(:message_id).distinct.size
  accurate = total_records == unique_users + 1
  puts accurate ? "All is OK" : "#{total_records - unique_users} duplicates found. Searching..."
  unless accurate
    IncomingMessage.select(:account, :message_id).group(:account, :message_id).having("count(*) > 1").each do |m|
      IncomingMessage.delete_duplicate(m.message_id, m.account)
      puts "#{m.account}: #{m.name}"
    end
  end
end

task :update_unique_key => :environment do
  Match.where(unique_key: nil).find_in_batches do |group|
    group.each do |m|
      AddUniqueKeyWorker.perform_async(m.name, m.account)
    end
  end
end

task :update_sexuality => :environment do
  queue = Match.where(account: "primal_objective", sexuality: nil)
  bar = ProgressBar.new(queue.size)
  queue.each do |m|
    m.update(sexuality: "Straight")
    bar.increment!
  end
end
