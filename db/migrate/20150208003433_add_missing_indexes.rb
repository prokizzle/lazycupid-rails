
class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :users, [:invited_by_id, :invited_by_type]
    add_index :preferences, :user_id
  end
end
