class AddDateToOutgoingVisits < ActiveRecord::Migration
  def change
    add_column(:outgoing_visits, :date, :time)
  end
end
