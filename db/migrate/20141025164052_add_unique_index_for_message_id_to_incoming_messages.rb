class AddUniqueIndexForMessageIdToIncomingMessages < ActiveRecord::Migration
  def change
    add_index :incoming_messages, [:message_id, :account], :unique => true
  end
end
