class AddUniqueUserNameAccountComboToMatches < ActiveRecord::Migration
  def change
    add_index :matches, [:name, :account], :unique => true
  end
end
