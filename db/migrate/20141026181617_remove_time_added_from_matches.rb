class RemoveTimeAddedFromMatches < ActiveRecord::Migration
  def change
    remove_column(:matches, :time_added)
    remove_column(:matches, :visitor_timestamp)
    remove_column(:matches, :visit_count)
    remove_column(:matches, :driving_distance)
    remove_column(:matches, :driving_duration)
  end
end
