class AddTimestampsToUsernameChanges < ActiveRecord::Migration
  def change
    add_column(:username_changes, :created_at, :datetime)
    add_column(:username_changes, :updated_at, :datetime)
  end
end
