class AddBodyTypeToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :body_type, :string
  end
end
