class AddUniqueIndexToMatches < ActiveRecord::Migration
  def change
    add_index :matches, :unique_key, :unique => true
  end
end
