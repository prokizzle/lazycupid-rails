class CreateSettings < ActiveRecord::Migration
  def change
    create_table(:settings, id: false, primary_key: 'account') do |t|
      t.integer :min_age
      t.integer :max_age
      t.integer :min_percent
      t.integer :max_distance
      t.integer :my_age
      t.string :my_gender
      t.string :my_sexuality
      t.integer :roll_frequency
      t.string :account
    end
  end
end
