class AddThumbToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :thumb, :string
  end
end
