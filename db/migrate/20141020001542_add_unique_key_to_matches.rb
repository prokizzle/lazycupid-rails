class AddUniqueKeyToMatches < ActiveRecord::Migration
  def change
    add_column :matches, :unique_key, :string, :unique => true
  end
end
