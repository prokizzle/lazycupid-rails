class AddTimestampsToOutgoingVisits < ActiveRecord::Migration
  def change
    add_column(:outgoing_visits, :created_at, :datetime)
    add_column(:outgoing_visits, :updated_at, :datetime)
  end
end
