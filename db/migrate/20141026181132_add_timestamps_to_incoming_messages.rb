class AddTimestampsToIncomingMessages < ActiveRecord::Migration
  def change
    add_column(:incoming_messages, :created_at, :datetime)
    add_column(:incoming_messages, :updated_at, :datetime)
  end
end
