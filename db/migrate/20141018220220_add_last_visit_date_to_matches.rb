class AddLastVisitDateToMatches < ActiveRecord::Migration
  def change
    add_column(:matches, :last_visit_time, :time)
  end
end
