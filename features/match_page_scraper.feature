Feature: Match Page Scraper
  In order to make sure the match page scraper works
  As a developer
  I want to make sure the scraper page works without error

Scenario: Scrape Matches for All Users
    When I perform a match scrape loop
    Then There should not be any errors