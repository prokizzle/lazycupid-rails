Feature: Inbox Parser
  In order to properly parse the inbox to ignore matches who have messaged you
  As a developer
  I want to make sure my regular expressions are up to date

Scenario: collect usernames from inbox
  Given I download the inbox page
  Then I should not see any errors when parsing the inbox page for usernames