Feature: Async Profile Parser
  In order to process profiles asynchronously
  As a user
  I want to make sure the parser works

Scenario: load a profile
  Given I request a profile derek_andrew with id 3
  Then I should receive a valid profile object

Scenario: a list name change
  Given I visit the profile of a name that has been changed
  Then a-list name change should be true
  And intended handle should be the old name
  And new handle should be the new name

Scenario: profile doesn't exist
  Given I visit a non-existent profile
  Then inactive should be true