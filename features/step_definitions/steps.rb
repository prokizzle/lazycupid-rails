def http_request(link, user_id)
  intended_handle = link.split('/').last
  p intended_handle
  begin
    user                  = User.find_by(id: user_id)
    cookies               = user.settings(:sessions).mechanize
    agent = Mechanize.new do |a|
      a.ssl_version = :TLSv1
    end
    agent.cookie_jar      = YAML::load(cookies)
    agent.user_agent      = user.settings(:personal).useragent
    url                   = URI.escape(link)
    agent.read_timeout    = 30
    page_object           = agent.get(link)
    page_object.encoding  = 'utf-8'
    # page_body             = page_object.parser.xpath("//body").to_html
    page_source           = page_object.parser.xpath("//html").to_s
    result_object        = {url: link.to_s, ready: true, username: intended_handle, source: page_source.to_s, inactive: false}
  rescue Exception => e
    result_object = {url: link, inactive: true, username: intended_handle, ready: true, error: e.message}
  end
  return result_object
end

def json_request(link, user_id)
  intended_handle = link.split('/').last
  p intended_handle
  begin
    user                  = User.find_by(id: user_id)
    cookies               = user.settings(:sessions).mechanize
    agent = Mechanize.new do |a|
      a.ssl_version = :TLSv1
    end
    agent.cookie_jar      = YAML::load(cookies)
    agent.user_agent      = user.settings(:personal).useragent
    url                   = URI.escape(link)
    agent.read_timeout    = 30
    page_object           = agent.get(link)
    result_object         = JSON.parse(page_object.body)
  rescue Exception => e
    result_object = {error: true, message: e.message}
  end

  return result_object
end

def handle_access_token(result, id)
  prefs = Match.preferences(id)
  if prefs[:sexuality] == 'straight'
    if prefs[:gender] == "M"
      they_want = 'men'
      i_want = 'women'
    else
      they_want = 'women'
      i_want = 'men'
    end
  else
    if prefs[:gender] == "M"
      they_want = 'men'
      i_want = 'men'
    else
      they_want = 'women'
      i_want = 'women'
    end
  end


  @html = Mechanize::Page.new(nil,{'content-type'=>'text/html'},result[:source],nil,Mechanize.new)
  access_token = @html.parser.search('script')[1].text.match(/ACCESS_TOKEN\s*=\s*"(.+)"/)[1]

  match_options = URI.encode({order_by: "LOGIN", i_want: i_want, they_want: they_want, last_login:3600, limit:1000}.to_json)

  match_url = "https://www.okcupid.com/apitun/match/search?&access_token=#{access_token}&_json=#{match_options}"

  request_id = UUIDTools::UUID.random_create
  return json_request(match_url, id)
end

Given(/^I download the inbox page$/) do
  @inbox = InboxParser.test_request("http://www.okcupid.com/messages")
  puts "Result downloaded"
  # p result
end

Then(/^I should not see any errors when parsing the inbox page for usernames$/) do
  InboxParser.scrape_inbox(1)
end

When(/^I visit a profile$/) do
  @name = "atx_male_unicorn"
  url = "http://www.okcupid.com/profile/#{@name}"
  @request_id = UUIDTools::UUID.random_create
  Browser.request(url, 7, @request_id)
  until Browser.async_response(@request_id)[:ready]
    a = 1
  end
  result = Browser.async_response(@request_id)
  Browser.delete_request(@request_id)

  @profile = Profile.parse(result)
end

Then(/^I should not see errors while parsing match criteria$/) do
  puts @profile[:ethnicity]
  puts @profile[:ethnicity].strip
  puts Match.meets_preferences?(@profile, 1)
end

When(/^I perform a match scrape$/) do
  response = http_request(MatchQueries.default_query, 13)
  @response = handle_access_token(response, 13)
end

Then(/^I want to see match results$/) do
  result = @response
  id = 13
  prefs = Match.preferences(13)
  account = Account.name(id)
  result["data"].each do |m|
    if prefs[:sexuality] == "gay"
      if prefs[:gender] == 'M'
        acceptable_gender = "M"
      else
        acceptable_gender = "F"
      end
    elsif prefs[:sexuality] == 'straight'
      if prefs[:gender] == 'M'
        acceptable_gender = 'F'
      else
        acceptable_gender = 'M'
      end
    end

    gender = m["gender"] == 1 ? "M" : "F"
    if gender == acceptable_gender
      puts m['username']
      puts m['gender'].is_a?String
      puts gender
    end

    # case m["orientation"].to_i
    # when 1
    #   sexuality = "Straight"
    # when 2
    #   sexuality = "Gay"
    # else
    #   sexuality = "Bisexual"
    # end
    # match = m['match'].to_i/100
    # AddMatchWorker.perform_async(account, m['username'], gender, id, "#{account}_#{m['username']}", {age: m['age'], city: m['city_name'], state: m['state_code'], match_percent: match.to_i/100, sexuality: sexuality})
  end
end

Then(/^I should get all the details$/) do
  puts @matches
end

Given(/^I request a profile ([-_\w\d]+) with id (\d)$/) do |user, id|
  url = "http://www.okcupid.com/profile/#{user}"
  @request_id = UUIDTools::UUID.random_create
  Browser.async_request(url, id, @request_id)
end

Then(/^I should receive a valid profile object$/) do
  until Browser.async_response(@request_id)[:ready]
    a = 1
  end
  result = Browser.async_response(@request_id)
  Browser.delete_request(@request_id)

  profile = Profile.parse(result)
end

Given(/^I visit the profile of a name that has been changed$/) do
  @old_name = "primal_objective"
  @new_name = "eloquentitis"
  url = "http://www.okcupid.com/profile/#{@old_name}"
  @request_id = UUIDTools::UUID.random_create
  Browser.async_request(url, 7, @request_id)
end

Then(/^a\-list name change should be true$/) do
  until Browser.async_response(@request_id)[:ready]
    a = 1
  end
  result = Browser.async_response(@request_id)
  @profile = Profile.parse(result)
  expect(  @profile[:a_list_name_change]).to eq(true)

end

Then(/^intended handle should be the old name$/) do

  expect(  @profile[:intended_handle]).to eq(@old_name)
end

Then(/^new handle should be the new name$/) do
  Browser.delete_request(@request_id)
  expect(  @profile[:new_handle]).to eq(@new_name)
end

Given(/^I visit a non\-existent profile$/) do
  url = "http://www.okcupid.com/profile/asdf"
  @request_id = UUIDTools::UUID.random_create
  Browser.async_request(url, 6, @request_id)
end

Then(/^inactive should be true$/) do
  until Browser.async_response(@request_id)[:ready]
    a = 1
  end
  result = Browser.async_response(@request_id)
  profile = Profile.parse(result)
  Browser.delete_request(@request_id)
  expect(profile[:inactive]).to eq(true)

end

When(/^I perform a match scrape loop$/) do
  @match_scraper_errors = false
  User.find_each do |user|
    if Roller.enabled?(user.id)
      print "#{user.settings(:personal).account} "
      puts Match.scrape(user.id).size
    end
  end
end

Then(/^There should not be any errors$/) do
  expect(@match_scraper_errors).to eq(false)
end
