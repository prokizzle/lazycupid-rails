# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 12.seconds do
#   unless $jobQueue.empty?
#     visit_job = $jobQueue.shift
#     runner "VisitorWorker.perform_async(#{visit_job[:username], visit_job[:account], visit_job[:id]})"
#   end
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
