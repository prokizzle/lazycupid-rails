require 'sidekiq'
require 'sidekiq-rate-limiter/version'
require 'sidekiq-rate-limiter/fetch'
require 'sidekiq/web'
require 'sidekiq-runner'


Sidekiq.configure_server do |config|
  Encoding.default_external = Encoding::UTF_8
  config.redis = {
    :url => ENV['redis_url'],
    :namespace => 'lazycupid'
  }
end

Sidekiq.configure_client do |config|
  Encoding.default_external = Encoding::UTF_8
  config.redis = {
    :url => ENV['redis_url'],
    :namespace => 'lazycupid'
  }
end

# Sidekiq.configure_server do |config|
#   config.error_handlers << lambda do |exception, context|
#     config.error_handlers << Proc.new {|ex,context| NewRelic::Agent.notice_error(ex, custom_params: context) }
#   end
# end


Sidekiq.configure_server do |config|
  Sidekiq.options[:fetch] = Sidekiq::RateLimiter::Fetch
end

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == ["admin", "6C8FNKPiUHCBXbXVrtes7ZaB"]
end

SidekiqRunner.configure do |config|
  config.add_instance('default') do |instance|
    instance.verbose = false
    instance.concurrency = 5
    instance.add_queue 'default'
  end
end

SidekiqRunner.configure_god do |god_config|
  god_config.interval = 30
  god_config.maximum_memory_usage = 700 # 4 GB.
end
