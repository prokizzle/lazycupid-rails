class String
  def titleize
    split(/(\W)/).map(&:capitalize).join
  end

  def has_text?(text)
    !(self =~ /#{text}/).nil?
  end
end

