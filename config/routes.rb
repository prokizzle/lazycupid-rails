Rails.application.routes.draw do
  devise_for :admins
  # if Rails.env.production?
    # devise_for :users, :controllers => { :registrations => "registrations" }
  # else
    devise_for :users
  # end

  # authenticated :user do
  # root :to => "preferences#index", as: :authenticated_root
  # get '/billing/*' => 'billing#index'
  # get '/billing' => 'billing#index'
  # get '/dashboard' => 'billing#index'
  # get '/user-details' => 'billing#user_details'
  post '/createSession' => 'sessions#create'
  # get '/createSession' => 'sessions#create'
  # get '/okcLogout' => 'sessions#destroy'
  # get '/profile/:id' => 'matches#visit'
  # post '/continuous_roll' => 'roll#continuous_roll'
  # post '/stop_rolling' => 'roll#stop_rolling'
  # get '/roll_status' => 'roll#roll_status'
  # get '/settings' => 'preferences#index'
  get '/api/get-preferences' => 'preferences#get_preferences'
  get '/api/current-user' => 'billing#current_user'
  post '/api/update-preferences' => 'preferences#update'
  # post '/ignore' => 'matches#ignore'
  get '/api/name-changes' => 'matches#name_changes'
  get '/api/random-name-change' => 'matches#name_change'
  # get '/more-matches' => 'matches#more_matches'
  # get '/reauthorize' => 'sessions#destroy'
  get '/get_user_id' => 'roll#get_user_id'
  # get '/roll-status' => 'roller#status'
  get '/queue' => 'roll#queue_size'
  get '/session-status' => 'roll#session_status'
  post '/api/authorize-card' => 'billing#add_card'
  post '/api/create-subscription' => 'billing#create_subscription'
  get '/api/account-name' => 'users#account_name'

  get '/msg_by_month' => 'stats#messages_by_month'
  get '/visits_by_month' => 'stats#visits_by_month'
  get '/api/billing' => 'users#billing'
  get '/api/next-action' => 'next_action#next'
  get '/api/next-unread' => 'next_action#new'
  get '/api/next-match' => 'next_action#mutual'
  get '/api/next-all' => 'next_action#all'
  get '/triage' => 'next_action#index'
  get '/name-changes' => 'username_change#index'

  root 'preferences#index'
  get '/login' => 'application#index'

  resources :matches, only: [:show, :index]
  # get '/users/sign_in' => 'application#index'
  # resources :users
  resources :incoming_messages, only: [:index]

  require 'sidekiq/web'
  require 'sidetiq/web'
  mount Sidekiq::Web => '/sidekiq'
  match '/stats' => 'matches#stats', via: :get



  get '/statusboard/user_status' => 'status_board#user_statuses'
  get '/statusboard/visits' => 'status_board#visits'
  get '/statusboard/messages' => 'status_board#messages'
  get '/statusboard/sidekiq' => 'status_board#queues'
  get '/statusboard/roller_queues' => 'status_board#roller_queues'
  get '/statusboard/conversions' => 'status_board#conversions'
  get '/statusboard/stats' => 'status_board#stats'
  get '/statusboard/processes' => 'status_board#processes'
  get '/statusboard/distances' => 'status_board#distances'


end
